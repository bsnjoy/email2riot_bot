#!/usr/bin/env python3
"""
    small script to check for unread count on imap inbox
"""
import imaplib
import email
import time
import base64
import traceback
import requests
import config

uid_max = 0

def log(text):
	print(time.strftime("%m.%d.%Y %H:%M:%S ") + text)

# Преобразует выражение вида
# =?utf-8?B?0KHQstC10YLQu9Cw0L3QsCBpbGZ1bW8=?= <buy@ilfumo.ru>
# в строку:
# Светлана ilfumo <buy@ilfumo.ru>
def decode(msg):
	msg_str = ''
	for part in msg.split():
		msg_decoded = email.header.decode_header(part)[0]
		if len(msg_str)!=0:
			msg_str += ' '
		if msg_decoded[1] is None:
			msg_str += msg_decoded[0]
		else: # обычно msg_decoded[1]='utf-8', но мало ли кто  пошлет в какой другой кодировке
			msg_str += msg_decoded[0].decode(encoding = msg_decoded[1])
	return msg_str

def riot(message):
	url = 'https://matrix.ilfumo.ru/_matrix/client/r0/rooms/' + config.room + '/send/m.room.message?access_token=' + config.bot_matrix_token
	payload = {'msgtype':'m.text','body':message}
	try:
		r = requests.post(url, json=payload)
	except Exception as e:
		print('error sending to riot', e)

def find_max_uid():
	global uid_max
	try:
		mail = imaplib.IMAP4_SSL(config.IMAPSERVER)
		mail.login(config.USER, config.PASSWORD)
		mail.select("inbox", True) # connect to inbox.
		result, mail_ids = mail.uid('search', None, 'UnSeen')
		uids = mail_ids[0].split()
		# Search for maximum uid
		uids_int = [int(s) for s in uids]
		if uids_int:
			uid_max = max(uids_int)
#		uid_max = 10/0
		print(uid_max)
		mail.close()
		mail.logout()
	except Exception as e:
		log("Сведения об исключении in find_max_uid: {}".format(e))
		print(traceback.format_exc())


def fetch_new_mails():
	global uid_max
	try:
		mail = imaplib.IMAP4_SSL(config.IMAPSERVER)
		mail.login(config.USER, config.PASSWORD)
		mail.select("inbox", True) # connect to inbox.
		result, mail_ids = mail.uid('search', None, 'UnSeen')
		uids = mail_ids[0].split()
		log( 'fetch_new_mails:' + repr(uids) )
		for uid in uids:
			if int(uid) > uid_max:
				uid_max = int(uid)
				print(uid)
				result, data = mail.uid('fetch', uid.decode(), '(RFC822)' )
				#print(result)
				#print(data)
				msg_raw = data[0][1]
#				msg = email.message_from_bytes(msg_raw, _class = email.message.EmailMessage)
				msg = email.message_from_bytes(msg_raw)
				email_subject = decode(msg['subject'])
				email_from = decode(msg['from'])
				print('From : ', email_from)
				print('Subject : ', email_subject)
				riot("From: {}\nSubject: {}".format(email_from, email_subject))
		mail.close()
		mail.logout()
	except Exception as e:
		msg = "Сведения об исключении in fetch_new_mails: {}".format(e)
		log(msg)
		print(traceback.format_exc())
		riot(msg)
		pass


find_max_uid()
while True:
	fetch_new_mails()
	log('uid_max=' + str(uid_max))
	time.sleep(60) # wait 60 seconds
